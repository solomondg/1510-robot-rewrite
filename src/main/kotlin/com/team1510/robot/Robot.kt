package com.team1510.robot

import edu.wpi.first.wpilibj.TimedRobot

class Robot : TimedRobot() {

    override fun robotInit() {}
    override fun teleopInit() {}
    override fun teleopPeriodic() {}
    override fun autonomousInit() {}
    override fun autonomousPeriodic() {}
}
