package com.team1510.robot.commands

import com.team1510.robot.subsystem.Drivetrain
import com.team2898.engine.motion.CheesyDrive
import edu.wpi.first.wpilibj.command.Command

class teleop: Command() {

    override fun initialize() {

    }

    override fun execute() {
        Drivetrain.drive(
                CheesyDrive.updateCheesy()
        )
    }

    override fun isFinished(): Boolean {
        return false
    }
}