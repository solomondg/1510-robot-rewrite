package com.team1510.robot

object Robotmap {
    //all the talon/can junk go here, all numbers are arbitrary and needs clarification
    val leftMasterDriveTalon = 1
    val rightMasterDriveTalon = 2
    val leftSlaveDriveTalon = 3
    val rightSlaveDriveTalon = 4
}